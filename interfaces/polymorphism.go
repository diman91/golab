// Package main - encrypt a struct using Golang's crypto library
//
package main

import (
	"fmt"
)

type Employee interface{}

type BankEmployee struct {
	name, password string
} 

type ItEmployee struct {
	name, password string
	id []int
}

type MallEmployee struct {
	// Not implemented yet
	// Just to show the use of .(type)
}

// This function accepts an interface , this can also be a struct, i.e. ItEmployee, BankEmployee
// We should just pass the employee address, and check for its actual type and then do what we need to
//
func print(empl interface{}) {
	switch empl.(type) {
	case ItEmployee:
		fmt.Println("Name: ", empl.(ItEmployee).name, "Pass: ", empl.(ItEmployee).password,"ID: ", empl.(ItEmployee).id[:])
	case BankEmployee:
		fmt.Println("Name: ", empl.(BankEmployee).name, "Pass:", empl.(BankEmployee).password)
	default:
		fmt.Printf("Ooops, choose an appropriate Employee and try again ... This (%T) is not implemented yet ... \n", empl)
	}
}


func main() {

	mysimplepass := []int{5, 4, 3, 3}	// ID = 5433

	var chris = ItEmployee{
		name : "Chris",
		password : "osfp.123",
		id : mysimplepass,
	}
	print(chris)

	//// wrong employee - not implemented yet
	//
	var nonExist = MallEmployee {}
	print(nonExist)
}

/* SAMPLE OUTPUT ::

$ go run polymorphism.go
Name:  Chris Pass:  osfp.123 ID:  [5 4 3 3]
Ooops, choose an appropriate Employee and try again ... This (main.MallEmployee) is not implemented yet ...

*/